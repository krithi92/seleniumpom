package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LeadsPage extends ProjectMethods{
	
	public LeadsPage() {
	       PageFactory.initElements(driver, this);	
	       
	}
	
	@FindBy(how =How.LINK_TEXT, using="CreateLead") WebElement eleCreateLead;
	
	public ViewLeadsPage clickCreateLead() {
	click(eleCreateLead);
	return new ViewLeadsPage();
	}
}
