package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LoginPage extends ProjectMethods{

public LoginPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="username") WebElement eleUsername;
	@FindBy(how = How.ID,using="password") WebElement elePassword;
	@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogin;
	
	public LoginPage enterUsername(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, data);
		return this; 
	}
	public LoginPage enterPassword(String data1) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, data1);
		return this;
	}
	public HomePage clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleLogin);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new HomePage();
	}
}













