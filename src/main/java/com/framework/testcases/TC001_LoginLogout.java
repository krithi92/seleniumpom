package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods {
	
//for all testcases in this design before test annotation is mandatory	

@BeforeTest
public void setData() {
testCaseName ="TC001_LoginLogout";
testDescription ="Login into leaftaps";
testNodes="Leads";
author ="Krithi";
category="Smoke";
dataSheetName="TC002";
}

@Test(dataProvider="fetchData")
public void login(String username, String password) {
new LoginPage()
.enterUsername(username)
.enterPassword(password)
.clickLogin();
//.clickLogout();

}	
}
