package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;

public class TC002_HomePage extends ProjectMethods {
	
	//for all testcases in this design before test annotation is mandatory	
	
	@BeforeTest
	public void setData() {
	testCaseName ="TC002_HomePage";
	testDescription ="Login into leaftaps";
	testNodes="Leads";
	author ="Krithi";
	category="Smoke";
	dataSheetName="TC002";
	
	}	
@Test(dataProvider="fetchData")
	
	public void createLead(String uname, String pwd) {
	new LoginPage()
	.enterUsername(uname)
	.enterPassword(pwd)
	.clickLogin();
	
	new HomePage()
	.HomePage();
			
}}
